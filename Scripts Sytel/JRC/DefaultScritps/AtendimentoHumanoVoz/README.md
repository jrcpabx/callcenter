## SCRIPT PADRÃO DE ATENDIMENTO POR VOZ
Este script pode ser utilizado para atendimento de voz receptiva ou ativa para atendimento humano.

Configurações através do arquivo **Config.xml** Como mostrado no exemplo abaixo:

```xml
	<config>
		<DataSources>
			<DataSource id = "historico" name = "SccReporterHistory" user = "root" pass = "root" />
			<DataSource id = "mailing" name = "MailingExemplo" user = "root" pass = "root" nomeTabela = "CONTATOS"/>	
		</DataSources>
		<outbound>
			<manualOutbound>false</manualOutbound>	
			<ringTimeout>20</ringTimeout> <!--Para chamadas ativas do tipo progressive ou manual outbound em que a chamada começa desconectada.-->	
		</outbound>	
		<agendamento>
			<horaMinima>08:00:00</horaMinima>
			<horaMaxima>20:00:00</horaMaxima>
			<diasAgendamento>60</diasAgendamento>
		</agendamento>	
		<protocoloAtendimento enabled="false">		
			<tagGravacao>1</tagGravacao> <!-- definir em qual tag de gravação será colocado o protocolo, colocar valores númericos. ex: TAG1 = 1-->
		</protocoloAtendimento>	
		<sytel>
			<controllerHost>sy-cc.jrcpabx.com.br</controllerHost>
			<user>admin</user>
			<password>jRC(2021)</password>		
		</sytel>
		<outcomesCustomizados enabled="false"/>	
	</config>
```

Abaixo uma descrição de cada elemento configurável neste documento:

* DataSources - Configuração de acesso ao banco de dados.
    - **id**: este ID não deve ser alterado, ele é referncia para dizer ao script qual é a conexão com a base de histórico de ligações da Sytel.
    - **name**: este é o nome da conexão ODBC a ser usada para acesso à base. Normalmente a mesma configurada no campaignManager.
    - **user**: Usuario do banco de dados.
    - **pass**: Senha do usuario do banco de dados.

* outbout - Configurações apenas para uso em campanhas ativas.
    - **manualOutbound**: valores válidos são true/false, para informar se a campanha é do tipo Manual Outbound. (Campanha ativa com discagem manual)
    - **ringTimeout**: tempo (em segundos) para aguardar o cliente atender a chamada.
* agendamento - Configurações do componente de agendamento da tela do atendente.
    - **horaMinima**: Horario minimo permitido de agendamento da chamada. (não ligar antes desse horario)
    - **horaMaxima**: Horario maximo permitido para agendamento de chamada. (não ligar depois desse horario) 
    - **diasAgendamento**: Numero de dias para frente (a partir do dia atual) que a interface irá permitir o atendente escolher para o agendamento. (para não permitir agendar a ligação para uma data muito distante)
* protocoloAtendimento - configurações de geração de protocolo de atendimento
    - **enabled**: habilitar/desabilitar geração de protocolo. valores válidos true/false.
    - **tagGravacao**: número da tag do arquivo de gravação a ser adicionado o protocolo de atendimento.
* sytel - configurações de integração com API da Sytel.
    - **controllerHost**: IP, Domínio ou nome do servidor para o qual a requisição à API da sytel será feita. (normalmente fica no servidor Controller da Sytel)
    - **user**: Usuário Sytel. (de preferencia usar um usuário apenas para intergrações que seja super usuário para ter acesso à API.)
    - **password**: Senha do usuário Sytel.
* outcomesCustomizados - Valores válidos são true/false. Se esta opção estiver como "true" vai passar a buscar as tabulações de uma tabela customizada na base histórica e não mais na API da Sytel. (usar apenas para clientes que querem manter os mesmos códigos de tabulação para campanhas diferentes).


## ATENÇÃO
A tela principal de atendimento contém 12 botões de tabulação. Para usar este script é necessário criar as tabulações no sistema da Sytel seguindo os seguintes códigos internos. A campanha precisará ter configurada as tabulações (outcomes) de código 3XY*[^1] no onde X é o numero da linha e Y numero da coluna da posição do botão na tela. 
Sendo assim os códigos de tabulação dos botões devem ser: 
* 311 para o botão da linha 1 coluna 1;
* 312 para o botão da linha 1 coluna 2;
* 321 para o botão da linha 2 coluna 1;
* 322 para o botão da linha 2 coluna 2;
* E assim sucessivamente.

